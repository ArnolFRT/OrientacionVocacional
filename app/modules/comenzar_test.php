<?php
// configs
include_once 'requires.php';
include_once 'Ctrl_Historial_Prueba.php';
?>
<html>
<?php
// templates
include 'header_template.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- content -->
<div class="wrapper">
    <?php include 'navbar_header_template.php'?>
    <?php include 'navbar_template.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php include 'page_title_template.php';?>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                            </div>
                            <div class="ibox-content">
                                <div class="ibox-content">
                                    <div class="list-group">
                                        <a class="list-group-item active">
                                            <h4 class="list-group-item-heading"><strong>Detalles del Test</strong></h4>
                                            <p class="list-group-item-text"><h4>A continuación, encontraras preguntas relacionadas con ambientes o actividades ocupacionales. Al contestarlas, responde pensando en QUE TANTO TE GUSTAN, dejando de lado si eres competente o no en esos espacios o actividades.</h4></p>
                                            <p class="list-group-item-text"><h4>Elige la opcion en base al número correspondiente a tu preferencia siguiendo las indicaciones de la tabla que tienes a continuación: </h4></p>
                                        </a>
                                        <a class="list-group-item">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <button type="button" class="btn btn-danger m-r-sm">1</button>
                                                        Me disgusta totalmente
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-warning m-r-sm">2</button>
                                                        Me disgusta
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default m-r-sm">3</button>
                                                        Me es indiferente
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-info m-r-sm">4</button>
                                                        Me gusta
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary m-r-sm">5</button>
                                                        Me gusta mucho
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </a>
                                    </div>
                                </div>
                                <form action="Ctrl_Test_Vocacional.php" method="get" class="form-horizontal">
                                    <div id="preguntas" class="form-group">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary block full-width m-b col-md-12 col-sm-12">Siguiente</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include 'footer_template.php'?>
</div>
</body>
<?php
// templates
include 'scripts_template.php';

?>
</html>
