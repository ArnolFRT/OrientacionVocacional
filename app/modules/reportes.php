<?php
// configs
include_once 'requires.php';
?>
<html>
<?php
// templates
include 'header_template.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- content -->
<div class="wrapper">
    <?php include 'navbar_header_template.php'?>
    <?php include 'navbar_template.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php include 'page_title_template.php';?>

        <!-- Main content -->
        <section class="content container-fluid">


            <ul class="nav navbar-nav navbar-left">
                <li class="navbar-title">Estudiantes que realizaron el Test</li>
                <div class="text-right col-md-3">
                    <a type="button" class="btn btn-success" href="pdf_estudiantes_test.php">REPORTE EN PDF</a>
                </div>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <div class="text-right col-md-3">
                    <a type="button" class="btn btn-success" href="excel_estudiantes_test.php">REPORTE EN EXCEL</a>
                </div>
            </ul>
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>CI</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Apto para Ingenieria</th>
                    <th>Carrera segun Test</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($estu as $es)
                {
                    echo '<tr>
                                        <td>'.$es["ci"].'</td>
                                        <td>'.$es["nombres"].'</td>
                                        <td>'.$es["apellidos"].'</td>
                                        <td>'.$es["curso"].'</td>
                                        <td>'.$es["telefono"].'</td>
                                        <td>'.$es["email"].'</td>
                                        <td>'.$es["sexo"].'</td>
                                      </tr>';
                }
                ?>
                </tbody>

            </table>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include 'footer_template.php'?>
</div>
</body>
<?php
// templates
include 'scripts_template.php';

?>
</html>
