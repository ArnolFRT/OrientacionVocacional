<?php
$pruebaInge=PruebasIngenieria::query()->where('id_estudiante','=',$dataUser['ciOid'])->get();
$pregContestadas=$pruebaInge->count();
if($pregContestadas>0)
{
    $aciertos=PruebasIngenieria::query()->where('id_estudiante','=',$dataUser['ciOid'])
        ->where('respuesta','=','2')->get()->count();
    $fallos=PruebasIngenieria::query()->where('id_estudiante','=',$dataUser['ciOid'])
        ->where('respuesta','=','-1')->get()->count();
    $notaExamen=$pruebaInge->sum('respuesta');
    $percen=$aciertos*100/30;
    $percenTxt="";
    if($percen>=0 && $percen<=25)
    {
        $percenTxt="MALO";
    }
    elseif ($percen>25 && $percen<=50)
    {
        $percenTxt="NORMAL";
    }
    elseif ($percen>50 && $percen<=75)
    {
        $percenTxt="BUENO";
    }
    elseif ($percen>75 && $percen<=100)
    {
        $percenTxt="MUY BUENO";
    }
}

$pruebaOV=Pruebas::query()->where('id_estudiante','=',$dataUser['ciOid'])->get();
$pregOV=$pruebaOV->count();

$fuzzyShow=Fuzzy::query()->where('id_estudiante','=',$dataUser['ciOid'])->get();

?>