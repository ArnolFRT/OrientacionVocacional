<?php
// configs
include_once 'requires.php';
include_once 'Ctrl_Historial_Prueba.php';
?>
<html>
<?php
// templates
include 'header_template.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- content -->
<div class="wrapper">
    <?php include 'navbar_header_template.php'?>
    <?php include 'navbar_template.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php include 'page_title_template.php';?>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class=""><a href="#examenPsicotecnico" data-toggle="tab">Examen Psicotécnico</a></li>
                            <?php
                            if($pregOV>0)
                            {
                                echo '<li class="active"><a href="#examenVocacional" data-toggle="tab">Examen Vocacional</a></li>';
                            }
                            ?>
                        </ul>
                        <div class="tab-content">
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="examenPsicotecnico">
                                <!-- The timeline -->
                                <ul class="timeline timeline-inverse">
                                    <!-- timeline time label -->
                                    <li class="time-label">
                                        <span class="bg-teal-gradient">
                                          Resultados del Examen Psicotécnico
                                        </span>
                                    </li>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">Número de preguntas:</a> 30
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">Preguntas contestadas:</a> <?php echo $pregContestadas ?>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">Preguntas sin contestar:</a> <?php echo 30-$pregContestadas ?>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">Tiempo utilizado:</a> <?php echo $pruebaInge[0]['tiempo'] ?>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">Aciertos:</a> <?php echo $aciertos ?>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">Fallos:</a> <?php echo $fallos ?>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">Nota del examen:</a> <?php echo $notaExamen ?>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>

                                        <div class="timeline-item">
                                            <h3 class="timeline-header no-border"><a href="#">DEBIDO AL TEST REALIZADO SE DETERMINO QUE USTED </a><?php echo $dataUser['nombres']." ".$dataUser['apellidos']?><a> SERA UN ESTUDIANTE </a><?php echo $percenTxt ?><a> PARA EL ÁREA DE INGENIERÍA. TRAS ESTE RESULTADO, DEBE REALIZAR EL SIGUIENTE TEST DEL CUESTIONARIO DE INTERÉS VOCACIONAL EL CUAL LE AYUDARA A ELEGIR EL ÁREA DE INGENIERÍA DE SU ESPECIALIDAD.</a>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline time label -->
                                </ul>
                            </div>
                            <!-- /.tab-pane -->
                            <!-- /.tab-pane -->
                            <div class="active tab-pane" id="examenVocacional">
                                <input type="hidden" id="res_TI" value="<?= $pruebaOV[0]["res_TI"] ?>">
                                <input type="hidden" id="res_MP" value="<?= $pruebaOV[0]["res_MP"] ?>">
                                <input type="hidden" id="res_DC" value="<?= $pruebaOV[0]["res_DC"] ?>">
                                <input type="hidden" id="res_CQI" value="<?= $pruebaOV[0]["res_CQI"] ?>">
                                <input type="hidden" id="res_COM" value="<?= $pruebaOV[0]["res_COM"] ?>">
                                <input type="hidden" id="res_MA" value="<?= $pruebaOV[0]["res_MA"] ?>">
                                <input type="hidden" id="res_CP" value="<?= $pruebaOV[0]["res_CP"] ?>">
                                <input type="hidden" id="res_CA" value="<?= $pruebaOV[0]["res_CA"] ?>">
                                <input type="hidden" id="res_AR" value="<?= $pruebaOV[0]["res_AR"] ?>">
                                <input type="hidden" id="res_CT" value="<?= $pruebaOV[0]["res_CT"] ?>">
                                <?php
                                if($pregOV>0)
                                {
                                    echo '
                                            <div id="testVoca"></div>
                                           
                                              <div class="small-box bg-green">
                                                <div class="inner">
                                                  <h3>'.$fuzzyShow[0]['valor'].'<sup style="font-size: 20px">%</sup></h3>
                                                  <p>La carrera acorde a sus aptitudes es '.$fuzzyShow[0]['carrera'].'</p>
                                                </div>
                                                <div class="icon">
                                                  <i class="ion ion-stats-bars"></i>
                                                </div>
                                              </div>
                                            
                                         ';

                                }
                                ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include 'footer_template.php'?>
</div>
</body>
<?php
// templates
include 'scripts_template.php';

?>
</html>
