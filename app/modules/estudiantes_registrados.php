<?php
// configs
include_once 'requires.php';
include 'Ctrl_Estudiante.php';
?>
<html>
<?php
// templates
include 'header_template.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- content -->
<div class="wrapper">
    <?php include 'navbar_header_template.php'?>
    <?php include 'navbar_template.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php include 'page_title_template.php';?>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Estudiantes Registrados</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example3" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>CI</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Curso</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            <th>Sexo</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($estu as $es)
                        {
                            echo '<tr>
                                        <td>'.$es["ci"].'</td>
                                        <td>'.$es["nombres"].'</td>
                                        <td>'.$es["apellidos"].'</td>
                                        <td>'.$es["curso"].'</td>
                                        <td>'.$es["telefono"].'</td>
                                        <td>'.$es["email"].'</td>
                                        <td>'.$es["sexo"].'</td>
                                      </tr>';
                        }
                        ?>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php include 'footer_template.php'?>
</div>
</body>
<?php
// templates
include 'scripts_template.php';

?>
</html>
