<?php
    use Illuminate\Database\Eloquent\Model;
    class Preguntas extends Model
    {
        protected $table='preguntas';

        protected $fillable=[
            'pregunta',
            'tipo',
            'estado',
            'image_path'
        ];

        public $incrementing=true;
        public $timestamps=false;
    }
?>